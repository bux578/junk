﻿namespace Junk
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbSourcePath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnBrowseSource = new System.Windows.Forms.Button();
            this.btnBrowseTarget = new System.Windows.Forms.Button();
            this.tbTargetPath = new System.Windows.Forms.TextBox();
            this.btnCreateJunction = new System.Windows.Forms.Button();
            this.lbSourceFolders = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // tbSourcePath
            // 
            this.tbSourcePath.Location = new System.Drawing.Point(12, 25);
            this.tbSourcePath.Name = "tbSourcePath";
            this.tbSourcePath.Size = new System.Drawing.Size(302, 20);
            this.tbSourcePath.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Select Mods Directory";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(400, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(247, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Select target directory that will contain the Junction";
            // 
            // btnBrowseSource
            // 
            this.btnBrowseSource.Location = new System.Drawing.Point(320, 23);
            this.btnBrowseSource.Name = "btnBrowseSource";
            this.btnBrowseSource.Size = new System.Drawing.Size(66, 23);
            this.btnBrowseSource.TabIndex = 4;
            this.btnBrowseSource.Text = "Browse ...";
            this.btnBrowseSource.UseVisualStyleBackColor = true;
            this.btnBrowseSource.Click += new System.EventHandler(this.btnBrowseSource_Click);
            // 
            // btnBrowseTarget
            // 
            this.btnBrowseTarget.Location = new System.Drawing.Point(711, 23);
            this.btnBrowseTarget.Name = "btnBrowseTarget";
            this.btnBrowseTarget.Size = new System.Drawing.Size(66, 23);
            this.btnBrowseTarget.TabIndex = 5;
            this.btnBrowseTarget.Text = "Browse ...";
            this.btnBrowseTarget.UseVisualStyleBackColor = true;
            this.btnBrowseTarget.Click += new System.EventHandler(this.btnBrowseTarget_Click);
            // 
            // tbTargetPath
            // 
            this.tbTargetPath.Location = new System.Drawing.Point(403, 25);
            this.tbTargetPath.Name = "tbTargetPath";
            this.tbTargetPath.Size = new System.Drawing.Size(302, 20);
            this.tbTargetPath.TabIndex = 6;
            // 
            // btnCreateJunction
            // 
            this.btnCreateJunction.Location = new System.Drawing.Point(403, 60);
            this.btnCreateJunction.Name = "btnCreateJunction";
            this.btnCreateJunction.Size = new System.Drawing.Size(102, 40);
            this.btnCreateJunction.TabIndex = 7;
            this.btnCreateJunction.Text = "Create Junctions";
            this.btnCreateJunction.UseVisualStyleBackColor = true;
            this.btnCreateJunction.Click += new System.EventHandler(this.btnCreateJunction_Click);
            // 
            // lbSourceFolders
            // 
            this.lbSourceFolders.FormattingEnabled = true;
            this.lbSourceFolders.Location = new System.Drawing.Point(12, 60);
            this.lbSourceFolders.Name = "lbSourceFolders";
            this.lbSourceFolders.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbSourceFolders.Size = new System.Drawing.Size(374, 212);
            this.lbSourceFolders.TabIndex = 8;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(817, 289);
            this.Controls.Add(this.lbSourceFolders);
            this.Controls.Add(this.btnCreateJunction);
            this.Controls.Add(this.tbTargetPath);
            this.Controls.Add(this.btnBrowseTarget);
            this.Controls.Add(this.btnBrowseSource);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbSourcePath);
            this.Name = "Main";
            this.Text = "Junk - Junction Creator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbSourcePath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnBrowseSource;
        private System.Windows.Forms.Button btnBrowseTarget;
        private System.Windows.Forms.TextBox tbTargetPath;
        private System.Windows.Forms.Button btnCreateJunction;
        private System.Windows.Forms.ListBox lbSourceFolders;
    }
}


﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Junk
{
    public partial class Main : Form
    {
        private string _lastSourcePath;
        private string _selectedTargetPath;

        public Main()
        {
            InitializeComponent();
        }

        private void btnBrowseSource_Click(object sender, EventArgs e)
        {
            using(var fbd = new FolderBrowserDialog())
            {
                fbd.SelectedPath = string.IsNullOrEmpty(_lastSourcePath) ? "" : _lastSourcePath;

                var result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    tbSourcePath.Text = fbd.SelectedPath;
                    ListMods();
                }
            }
        }

        private void ListMods()
        {
            var di = new DirectoryInfo(tbSourcePath.Text);
            var allMods = di.GetDirectories();
            foreach (var modDi in allMods)
            {
                lbSourceFolders.Items.Add(modDi.FullName);
            }
        }

        private void btnBrowseTarget_Click(object sender, EventArgs e)
        {
            using(var fbd = new FolderBrowserDialog())
            {

                fbd.SelectedPath = string.IsNullOrEmpty(_selectedTargetPath) ? "" : _selectedTargetPath;

                var result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    _selectedTargetPath = fbd.SelectedPath;
                    tbTargetPath.Text = _selectedTargetPath;
                }
            }
        }


        private void btnCreateJunction_Click(object sender, EventArgs e)
        {

            if (lbSourceFolders.SelectedItems.Count == 0)
            {
                MessageBox.Show("No Folders Selected");
                return;
            }

            if (string.IsNullOrEmpty(_selectedTargetPath))
            {
                MessageBox.Show("No Target Folder Selected");
                return;
            }

            foreach (string sourcePath in lbSourceFolders.SelectedItems)
            {
                var sourceDir = new DirectoryInfo(sourcePath);
                var targetDir = new DirectoryInfo(_selectedTargetPath);

                JunctionPoint.Create(sourceDir.FullName, Path.Combine(targetDir.FullName, sourceDir.Name), true);                
            }

        }
    }
}
